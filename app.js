const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const request = require("request")
const https = require("https");

var coins = new Object();
var time = new Date().toUTCString().substr(0, 25);
var news = new Object();
var news_time = new Date().getTime();

function checkCryptoPanic() {
  request({
    url: 'http://cryptopanic.com/api/posts/?auth_token=a83fefb2bd060bb172866b13e8c360edc9c067a0&filter=trending',
    json: true
  }, function(error, response, body) {
    if(error) console.log('error: ' + error);

    console.log(JSON.stringify(body));

    news = JSON.parse(JSON.stringify(body));
    var time = new Date(news.results[0].published_at).getTime();
    if(time != news_time)
    {
      news_time = time;
      postArticle(news.results[0].url);
    }
  });
}

function postArticle(url) {
  client.send_message(client.get_channel('403243536071524355'), url);
}

function json() {
  request({
    url: 'https://api.coinmarketcap.com/v1/ticker/?limit=0',
    json: true
  }, function(error, response, body) {
    coins = JSON.parse(JSON.stringify(body));
    time = new Date().toUTCString().substr(0, 25);
  });
}

//checkCryptoPanic();
json();
setInterval(json, 600000); // 10 Minutes
//setInterval(checkCryptoPanic, 600000); // 10 Minutes

client.on("ready", () => {
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`);
  client.user.setGame(`on ${client.guilds.size} servers`);
});

client.on("message", async message => {

  if (message.author.bot) return;
  if (message.content.indexOf(config.prefix) !== 0) return;

  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  if (command === "ping") {
    const m = await message.channel.send("Ping?");
    m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
  }

  if (command === "say") {
    const sayMessage = args.join(" ");
    message.delete().catch(O_o => {});
    message.channel.send(sayMessage);
  }

  if (command === "purge") {
    const deleteCount = parseInt(args[0], 10);
    if (!deleteCount || deleteCount < 2 || deleteCount > 100)
      return message.reply("Please provide a number between 2 and 100 for the number of messages to delete");
    const fetched = await message.channel.fetchMessages({
      count: deleteCount
    });
    message.channel.bulkDelete(fetched)
      .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
  }

  if (command === "top") {
    var array = "";
    array += "**Top 20 Coins**\n```";
    for (var i = 0; i < 20; i++) {
      array += coins[i].rank + ". " + coins[i].name + " " + coins[i].price_usd + " USD" + "\n";
    }
    array += "Updated at " + time + "```";
    message.channel.send(array);
  }

  if (command === "coin") {
    const coin = args.join(" ").toUpperCase();
    for (var i = 0; i < coins.length; i++) {
      if (coins[i].name.toUpperCase() == coin || coins[i].symbol.toUpperCase() == coin) {
        var msg = '**' + coins[i].rank + '. ' + coins[i].name + ' (' + coins[i].symbol + ')**' + '\n'
        + '```Change (24h): ' + coins[i].percent_change_24h + '%\n'
        + 'Price (USD): ' + coins[i].price_usd + ' USD\n'
        + 'Price (BTC): ' + coins[i].price_btc + ' BTC\n'
        + '24h Volume: ' + coins[i]['24h_volume_usd'] + ' USD\n'
        + 'Updated at ' + time + '```';
        message.channel.send(msg);
        break;
      }
    }
  }
});

client.login(config.token);
